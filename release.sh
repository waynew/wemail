#!/bin/sh

python3 setup.py bdist_wheel 
if [ $? -ne 0 ]; then
    echo 'Failed to build wheel'
    exit 1
fi

twine check dist/wemail-(python3.7 wemail.py --version)-py3-none-any.whl
if [ $? -ne 0]; then 
    echo "Twine check failed"
    exit 1
fi

twine upload --config-file ~/.pypirc -r wemail dist/wemail-$(python3.7 wemail.py --version)-py3-none-any.whl
if [ $? -ne 0]; then
    echo "Twine upload failed!"
    exit 1
fi

git tag $(python3 wemail.py --version)
if [ $? -eq 0]; then
    echo "Release succeeded!"
fi
